package Game;

import java.io.IOException;

public class main {

	private static void help() {
		System.out.println("Player icon: @");
		System.out.println("Robot icon: R");
		System.out.println("Hazards: | , X");
		System.out.println("Your goal is to not get caught by the robot (R)!");
		System.out.println("Try to kill the robot by baiting it into a hazard!");
		System.out.println(
				"But be careful the robot learns from it's mistake so you can't easily kill it with the same metod twice!");
		System.out.println("Move Forward: W");
		System.out.println("Move Backwards: S");
		System.out.println("Move Left: A");
		System.out.println("Move Right: D");
		System.out.println("Stay in a position: Stay");
		System.out.println("Exit game: exit");
	}

	private static void menu() {
		System.out.println("-------------------");
		System.out.println("|WELCOME TO ROBOT!|");
		System.out.println("-------------------");
		System.out.println("1) Start the game");
		System.out.println("2) How to play");
		System.out.println("exit) Exit");
	}

	public static void main(String[] args) {
		String usrinput = "";
		menu();
		Player player = new Player(3, 3);
		Map map = new Map(player);
		Robot brainiac = new Robot(map, player, 0, 0);
		while (!usrinput.matches("3")) {
			try {
				usrinput = HelpingFunctions.readFromKeyboard();
				switch (usrinput) {
				case "1":
					brainiac.printRobotMap();
					// map.printMap();
					while (!usrinput.matches("exit")) {
						try {
							usrinput = HelpingFunctions.readFromKeyboard();
						} catch (IOException e) {

						}
						switch (usrinput) {
						case "s":
							player.movePlayerDown();
							brainiac.chase();
							brainiac.printRobotMap();
							break;
						case "w":
							player.movePlayerUp();
							brainiac.chase();
							brainiac.printRobotMap();
							break;
						case "a":
							player.movePlayerLeft();
							brainiac.chase();
							brainiac.printRobotMap();
							break;
						case "d":
							player.movePlayerRight();
							brainiac.chase();
							brainiac.printRobotMap();
							break;
						case "stay":
							brainiac.chase();
							brainiac.printRobotMap();
							break;
						case "exit":
							System.exit(0);
						default:
							System.out.println("Wrong input!");
						}
					}
				case "2":
					help();
					break;
				case "exit":
					System.exit(0);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
