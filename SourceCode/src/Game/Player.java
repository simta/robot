package Game;

public class Player {

	public int posX;
	public int posY;

	public int trapPlaceX;
	public int trapPlaceY;

	Player(int posX, int posY) {
		this.posX = posX;
		this.posY = posY;
	}

	public void movePlayerDown() {
		posY++;
	}

	public void movePlayerUp() {
		posY--;
	}

	public void movePlayerLeft() {
		posX--;
	}

	public void movePlayerRight() {
		posX++;
	}

}
