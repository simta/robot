package Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class HelpingFunctions {

	
	public static int randomNumberGenerator(int min, int max) {
		Random r = new Random();
		return r.nextInt(max - min) + min;
	}
	
	public static String readFromKeyboard() throws IOException {
		String str = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		str = br.readLine();
		return str;
	}

}
