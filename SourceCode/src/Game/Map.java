package Game;

import java.util.ArrayList;

public class Map {

	public int myMapSizeX = 7;
	public int myMapSizeY = 7;

	public class Node {
		private char visual;
		private boolean walkable;
		private boolean playerhere = false;
		private boolean robothere = false;

		public boolean isRobothere() {
			return robothere;
		}

		public void setRobothere(boolean robothere) {
			this.robothere = robothere;
		}

		public boolean isPlayerhere() {
			return playerhere;
		}

		public void setPlayerhere(boolean playerhere) {
			this.playerhere = playerhere;
		}

		public char getVisual() {
			return visual;
		}

		public void setVisual(char visual) {
			this.visual = visual;
		}

		public boolean isWalkable() {
			return walkable;
		}

		public void setWalkable(boolean walkable) {
			this.walkable = walkable;
		}

	}

	public Node[][] myMap = new Node[myMapSizeX][myMapSizeY];
	private ArrayList<Character> mapHazards = new ArrayList<Character>();
	private static int maxNumberOfHazards = 10;
	private boolean hazardsGenerated = false;

	Player player;

	Map(Player player) {
		this.player = player;
		generateMap();
		addHazards();
		generateHazards();
		drawPlayer();
	}

	private void addHazards() {
		mapHazards.add('|');
		mapHazards.add('X');
	}

	private void generateMap() {
		for (int i = 0; i < myMapSizeX; i++) {
			for (int j = 0; j < myMapSizeY; j++) {
				Node n = new Node();
				n.setVisual('0');
				n.setWalkable(true);
				myMap[i][j] = n;
			}
		}
	}

	private void generateHazards() {
		int retryCounter = 0;
		if (hazardsGenerated == false) {
			int i = 0;
			while (i < maxNumberOfHazards) {
				int hazardPosX = HelpingFunctions.randomNumberGenerator(0, myMapSizeX - 1);
				int hazardPosY = HelpingFunctions.randomNumberGenerator(0, myMapSizeY - 1);
				if (myMap[hazardPosX][hazardPosY].isWalkable() || !myMap[hazardPosX][hazardPosY].isPlayerhere()
						|| !myMap[hazardPosX][hazardPosY].isRobothere()) {
					myMap[hazardPosX][hazardPosY]
							.setVisual(mapHazards.get(HelpingFunctions.randomNumberGenerator(0, mapHazards.size())));
					myMap[hazardPosX][hazardPosY].setWalkable(false);
					i++;
					// System.out.println("Hazard " + i + " added");
				} else {
					i--;
					retryCounter++;
					// System.out.println("Player Found Retry");
				}
				if (retryCounter == 200) {
					i = maxNumberOfHazards;
					break;
				}

			}
		}
		hazardsGenerated = true;
	}

	private void checkPlayerDeath() {
		for (int i = 0; i < myMapSizeX; i++) {
			for (int j = 0; j < myMapSizeY; j++) {
				if (!myMap[i][j].isWalkable() && i == player.posX && j == player.posY) {
					System.out.println("You walked into a trap!");
					System.exit(0);
				}
			}
		}
	}

	private void checkPlayerPos() {
		if (player.posY > myMapSizeY - 1) {
			player.posY = 0;
		} else if (player.posX > myMapSizeX - 1) {
			player.posX = 0;
		}
		if (player.posY < 0) {
			player.posY = myMapSizeY - 1;
		} else if (player.posX < 0) {
			player.posX = myMapSizeX - 1;
		}
		checkPlayerDeath();
	}

	public void clearMap() {
		for (int i = 0; i < myMapSizeX; i++) {
			for (int j = 0; j < myMapSizeY; j++) {
				if (!myMap[i][j].isWalkable()) {

				} else {
					myMap[i][j].setVisual('0');
					myMap[i][j].setPlayerhere(false);
				}
			}
		}
	}

	private void drawPlayer() {
		Node p = new Node();
		p.walkable = true;
		p.visual = '@';
		myMap[player.posX][player.posY] = p;
		myMap[player.posX][player.posY].setPlayerhere(true);
	}

	public void printMap() {
		// clearMap();
		checkPlayerPos();
		drawPlayer();
		System.out.println("Map: ");
		for (int i = 0; i < myMapSizeX; i++) {
			for (int j = 0; j < myMapSizeY; j++) {
				System.out.print(myMap[j][i].visual + " ");
			}
			System.out.print("\n");
		}
		System.gc();
	}

}
