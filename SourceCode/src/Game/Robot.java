package Game;

import java.util.ArrayList;

public class Robot {

	private class Node {
		int ID;
		int cost;
		int heur;
		int coordX;
		int coordY;
		boolean walkable;
		boolean playerhere;
		boolean robothere;

		public void setCost(int cost) {
			this.cost = cost;
		}

		public void setHeur(int heur) {
			this.heur = heur;
		}
	}

	public Map map;
	public Player player;

	public boolean alive;

	public int braniacPosX;
	public int braniacPosY;

	private static int mapSizeX = 7;
	private static int mapSizeY = 7;

	private int horizontalCost = 10;
	private int verticalCost = 10;
	private int diagonalCost = 10;

	private int robotLives = 5;

	ArrayList<Character> foundHazards = new ArrayList<Character>();
	ArrayList<String> speak = new ArrayList<String>();
	ArrayList<Node> openList = new ArrayList<Node>();
	ArrayList<Node> closedList = new ArrayList<Node>();
	public Node[][] robotMap = new Node[mapSizeX][mapSizeY];

	Robot(Map map, Player player, int braniacPosX, int braniacPosY) {
		this.map = map;
		this.player = player;
		this.braniacPosX = braniacPosX;
		this.braniacPosY = braniacPosY;
		loadSpeakModule();
		generateMap();
		calcHeuristic();
	}

	private void loadSpeakModule() {
		speak.add("I learn!");
		speak.add("A minor setback!");
		speak.add("Only a fleshwound!");
		speak.add("I learn from my mistakes. Are you?");
	}

	private void generateMap() {
		int tmp = 0;
		for (int i = 0; i < mapSizeX; i++) {
			for (int j = 0; j < mapSizeY; j++) {
				Node n = new Node();
				n.ID = tmp;
				n.coordX = i;
				n.coordY = j;
				n.heur = 0;
				n.walkable = true;
				robotMap[i][j] = n;
				tmp++;
			}
		}
	}

	private void calcHeuristic() {
		int distance = Math.abs(((braniacPosX - player.posX) + (braniacPosY - player.posY)));
		// System.out.println("Distance: " + distance);
		for (int i = 0; i < mapSizeX; i++) {
			for (int j = 0; j < mapSizeY; j++) {
				if (i == player.posX && j == player.posY) {
					robotMap[i][j].setHeur(0);
				} else {
					robotMap[i][j].setHeur(Math.abs(i - player.posX) + Math.abs(j - player.posY));
				}
			}
		}
	}

	private void addToOpen() {
		for (int i = 0; i < map.myMapSizeX; i++) {
			for (int j = 0; j < map.myMapSizeY; j++) {
				if (i == braniacPosX && j == braniacPosY) {
					// System.out.println("Map size: " + map.myMapSizeX + " : "
					// + map.myMapSizeY);
					if (i > 0 && i < map.myMapSizeX - 1 && j > 0 && j < map.myMapSizeY - 1) {
						openList.add(robotMap[i][j]);
						robotMap[i - 1][j - 1].setCost(verticalCost);
						openList.add(robotMap[i - 1][j - 1]);
						robotMap[i][j - 1].setCost(horizontalCost);
						openList.add(robotMap[i][j - 1]);
						robotMap[i + 1][j - 1].setCost(diagonalCost);
						openList.add(robotMap[i + 1][j - 1]);
						robotMap[i - 1][j].setCost(horizontalCost);
						openList.add(robotMap[i - 1][j]);
						robotMap[i + 1][j].setCost(horizontalCost);
						openList.add(robotMap[i + 1][j]);
						robotMap[i - 1][j + 1].setCost(diagonalCost);
						openList.add(robotMap[i - 1][j + 1]);
						robotMap[i][j + 1].setCost(verticalCost);
						openList.add(robotMap[i][j + 1]);
						robotMap[i + 1][j + 1].setCost(diagonalCost);
						openList.add(robotMap[i + 1][j + 1]);
					} else {
						if (i == 0 && j == 0) {
							// System.out.println("BF");
							openList.add(robotMap[i][j]);
							robotMap[i + 1][j].setCost(horizontalCost);
							openList.add(robotMap[i + 1][j]);
							robotMap[i][j + 1].setCost(verticalCost);
							openList.add(robotMap[i][j + 1]);
							robotMap[i + 1][j + 1].setCost(diagonalCost);
							openList.add(robotMap[i + 1][j + 1]);
						} else if (i == map.myMapSizeX - 1 && j == map.myMapSizeY - 1) {
							// System.out.println("JA");
							openList.add(robotMap[i][j]);
							robotMap[i - 1][j - 1].setCost(verticalCost);
							openList.add(robotMap[i - 1][j - 1]);
							robotMap[i][j - 1].setCost(horizontalCost);
							openList.add(robotMap[i][j - 1]);
							robotMap[i - 1][j].setCost(horizontalCost);
							openList.add(robotMap[i - 1][j]);
						} else if (i == 0 && j == map.myMapSizeY - 1) {
							// System.out.println("BA");
							openList.add(robotMap[i][j]);
							robotMap[i][j - 1].setCost(horizontalCost);
							openList.add(robotMap[i][j - 1]);
							robotMap[i + 1][j - 1].setCost(diagonalCost);
							openList.add(robotMap[i + 1][j - 1]);
							robotMap[i + 1][j].setCost(horizontalCost);
							openList.add(robotMap[i + 1][j]);
						} else if (i == map.myMapSizeX - 1 && j == 0) {
							// System.out.println("JF");
							openList.add(robotMap[i][j]);
							robotMap[i - 1][j].setCost(horizontalCost);
							openList.add(robotMap[i - 1][j]);
							robotMap[i - 1][j + 1].setCost(diagonalCost);
							openList.add(robotMap[i - 1][j + 1]);
							robotMap[i][j + 1].setCost(verticalCost);
							openList.add(robotMap[i][j + 1]);
						} else if (i > 0 && i < map.myMapSizeX && j == 0) {
							openList.add(robotMap[i][j]);
							robotMap[i - 1][j].setCost(horizontalCost);
							openList.add(robotMap[i - 1][j]);
							robotMap[i + 1][j].setCost(horizontalCost);
							openList.add(robotMap[i + 1][j]);
							robotMap[i - 1][j + 1].setCost(diagonalCost);
							openList.add(robotMap[i - 1][j + 1]);
							robotMap[i][j + 1].setCost(verticalCost);
							openList.add(robotMap[i][j + 1]);
							robotMap[i + 1][j + 1].setCost(diagonalCost);
							openList.add(robotMap[i + 1][j + 1]);
						} else if (i > 0 && i < map.myMapSizeX && j == map.myMapSizeY - 1) {
							openList.add(robotMap[i][j]);
							robotMap[i - 1][j - 1].setCost(verticalCost);
							openList.add(robotMap[i - 1][j - 1]);
							robotMap[i][j - 1].setCost(horizontalCost);
							openList.add(robotMap[i][j - 1]);
							robotMap[i + 1][j - 1].setCost(diagonalCost);
							openList.add(robotMap[i + 1][j - 1]);
							robotMap[i - 1][j].setCost(horizontalCost);
							openList.add(robotMap[i - 1][j]);
							robotMap[i + 1][j].setCost(horizontalCost);
							openList.add(robotMap[i + 1][j]);
						} else if (j > 0 && j < map.myMapSizeY && i == 0) {
							openList.add(robotMap[i][j]);
							robotMap[i][j - 1].setCost(horizontalCost);
							openList.add(robotMap[i][j - 1]);
							robotMap[i + 1][j - 1].setCost(diagonalCost);
							openList.add(robotMap[i + 1][j - 1]);
							robotMap[i + 1][j].setCost(horizontalCost);
							openList.add(robotMap[i + 1][j]);
							robotMap[i][j + 1].setCost(verticalCost);
							openList.add(robotMap[i][j + 1]);
							robotMap[i + 1][j + 1].setCost(diagonalCost);
							openList.add(robotMap[i + 1][j + 1]);
						} else if (j > 0 && j < map.myMapSizeY && i == map.myMapSizeX - 1) {
							openList.add(robotMap[i][j]);
							robotMap[i - 1][j - 1].setCost(verticalCost);
							openList.add(robotMap[i - 1][j - 1]);
							robotMap[i][j - 1].setCost(horizontalCost);
							openList.add(robotMap[i][j - 1]);
							robotMap[i - 1][j].setCost(horizontalCost);
							openList.add(robotMap[i - 1][j]);
							robotMap[i - 1][j + 1].setCost(diagonalCost);
							openList.add(robotMap[i - 1][j + 1]);
							robotMap[i][j + 1].setCost(verticalCost);
							openList.add(robotMap[i][j + 1]);
						}
					}
				}
			}
		}
	}

	private void removeUnwalkable() {
		for (int i = 0; i < openList.size(); i++) {
			if (openList.get(i).walkable == false) {
				closedList.add(openList.get(i));
				openList.get(i).setCost(openList.get(i).cost + 99);
				openList.remove(i);
			}
		}
	}

	private void findCheapestPlace() {
		removeUnwalkable();
		braniacPosX = openList.get(0).coordX;
		braniacPosY = openList.get(0).coordY;
		//System.out.println("OpenCoords: " + braniacPosX + " : " + braniacPosY);
		robotMap[braniacPosX][braniacPosY].robothere = false;
		closedList.add(openList.get(0));
		openList.remove(0);
		int smallest = openList.get(0).cost + openList.get(0).heur;
		int temp = 0;
		for (int i = 0; i < openList.size(); i++) {
			if (smallest > openList.get(i).cost + openList.get(i).heur) {
				smallest = openList.get(i).cost + openList.get(i).heur;
				temp = i;
			}
		}
		braniacPosX = openList.get(temp).coordX;
		braniacPosY = openList.get(temp).coordY;
		robotMap[braniacPosX][braniacPosY].robothere = true;
		//System.out.println("New Cheapest Value: " + smallest);
		//System.out.println("New position: " + braniacPosX + " : " + braniacPosY);
		checkRobotDeath();
		openList.clear();
	}

	private void clearMemory() {
		for (int i = 0; i < map.myMapSizeX; i++) {
			for (int j = 0; j < map.myMapSizeY; j++) {
				robotMap[i][j].setCost(0);
			}
		}
		openList.clear();
	}

	public void chase() {
		calcHeuristic();
		addToOpen();
		findCheapestPlace();
	}

	private void printRobot() {
		map.myMap[braniacPosX][braniacPosY].setVisual('R');
	}

	private void checkRobotDeath() {
		if (!map.myMap[braniacPosX][braniacPosY].isWalkable()) {
			System.out.println(speak.get(HelpingFunctions.randomNumberGenerator(0, speak.size())));
			foundHazards.add(map.myMap[braniacPosX][braniacPosY].getVisual());
			for (int j = 0; j < foundHazards.size(); j++) {
				System.out.println("So: " + foundHazards.get(j) + " are dangerous!");
			}
			for (int i = 0; i < mapSizeX; i++) {
				for (int j = 0; j < mapSizeY; j++) {
					for (int k = 0; k < foundHazards.size(); k++) {
						if (map.myMap[i][j].getVisual() == foundHazards.get(k)) {
							robotMap[i][j].walkable = false;
						}
					}
				}
			}
			System.out.println("Braniac died");
			braniacPosX = 0;
			braniacPosY = 0;
			robotLives--;
			clearMemory();
		}
		if (robotLives == 0) {
			System.out.println("YOU'RE WINNER!");
			System.exit(0);
		}
	}

	private void checkVictory() {
		if (braniacPosX == player.posX && braniacPosY == player.posY) {
			System.out.println("Gotcha!");
			System.exit(0);
		}
	}

	private void debugInfo() {
		System.out.println("RobotMap: ");
		for (int i = 0; i < map.myMapSizeX; i++) {
			for (int j = 0; j < map.myMapSizeY; j++) {
				System.out.print(robotMap[j][i].heur + " ");
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		for (int i = 0; i < map.myMapSizeX; i++) {
			for (int j = 0; j < map.myMapSizeY; j++) {
				System.out.print(robotMap[j][i].cost + " ");
			}
			System.out.print("\n");
		}
	}

	public void printRobotMap() {
		// System.out.println("Target at: " + player.posX + " : " +
		// player.posY);
		// System.out.println("I am at: " + braniacPosX + " : " + braniacPosY);
		// debugInfo();
		System.out.println("Robot Lives: " + robotLives);
		map.clearMap();
		printRobot();
		map.printMap();
		//debugInfo();
		checkVictory();
		clearMemory();
	}
}
